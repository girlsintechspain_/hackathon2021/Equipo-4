import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Playground from "./pages/playground";

import { Home } from "./pages/home";
import { WeekDay } from "./pages/week-day";
import { Duration } from "./pages/duration";
import { FirstSession } from "./pages/first-session";
import { Name } from "./pages/name";
import { Finish } from "./pages/finish";
import { Calendar } from "./pages/calendar";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/week-day">
          <WeekDay />
        </Route>
        <Route exact path="/duration">
          <Duration />
        </Route>
        <Route exact path="/first-session">
          <FirstSession />
        </Route>
        <Route exact path="/name">
          <Name />
        </Route>
        <Route exact path="/finish">
          <Finish />
        </Route>
        <Route exact path="/calendar">
          <Calendar />
        </Route>

        <Route exact path="/playground">
          <Playground />
        </Route>
      </Switch>
    </Router>
  );
}

export { App };
