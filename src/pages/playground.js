import { useEffect, useState } from "react";
import { database, messaging } from "../firebase.config"
import Button from '@mui/material/Button';

function App() {
  const [name, setName] = useState();
  const [age, setAge] = useState();
  const [isToken, setIsToken] = useState()

  useEffect(() => {
    messaging.getToken({vapidKey: "BJp8VGwyLzJSScFbHyORcIyuf_461YMxdV2U3XvErvFvkOMBCSRLi-hLqzxB89Urph3jCC3A1s8JHaplMH61ONA"}).then(console.log)
  }, [])

  // Push Function
  const Push = () => {
    database
      .ref("notifKey")
      .push({
        key: name
      })
      .catch(alert);
  };

  return (
    <div className="App" style={{ marginTop: 250 }}>
      <center>
        <input
          placeholder="Enter your name"
          value={name}
          onChange={(e) => setName(e.target.value)}
        />
        <br />
        <br />
        <input
          placeholder="Enter your age"
          value={age}
          onChange={(e) => setAge(e.target.value)}
        />
        <br />
        <br />
        <button onClick={Push}>PUSH</button>
        <Button variant="contained">Hello World</Button>
        {isToken && "has permission"}
      </center>
    </div>
  );
}

export default App;