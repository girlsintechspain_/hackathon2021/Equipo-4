import { Box } from "@mui/material";
import { Link } from "react-router-dom";
import Check from "@mui/icons-material/Check";

import "./finish.css";

function Finish() {
  return (
    <Box className="main-container">
      <Box>
        <p className="highlighted">¡Ya lo tienes!</p>
        <p className="text-b">
          Se ha creado el ciclo y recibirás notificaciones para acompañarte en
          todo momento.
        </p>
      </Box>
      <Link
        className="main-button finish-button"
        variant="contained"
        size="large"
        to="/calendar"
      >
        Entendido <Check fontSize="large" />
      </Link>
    </Box>
  );
}

export { Finish };
