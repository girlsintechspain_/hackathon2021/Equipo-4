import { Box } from "@mui/material"
import { Link } from "react-router-dom"
import Arrow from '@mui/icons-material/ArrowForward';

import "./home.css"

function Home() {
  return (
    <Box className="main-container">
      <Box>
        <p className={'text-a'}>Hola,</p>
        <p className={'text-a'}>Esto es</p>
        <p className={'highlighted'}>CICLOS</p>
      </Box>
      <Link className={'main-button'} variant="contained" size="large" to="/name">
        Comenzar <Arrow fontSize="large"/>
      </Link>
    </Box>
  )
}

export { Home }
